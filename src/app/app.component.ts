import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import Amplify from '@aws-amplify/core';
import PubSub, { AWSIoTProvider } from '@aws-amplify/pubsub';
import * as data from "./data.json"
import Auth from "@aws-amplify/auth"

Amplify.addPluggable(new AWSIoTProvider({
  aws_pubsub_region: 'us-east-1',
  aws_pubsub_endpoint: 'wss://a2hvzcojdkdai3-ats.iot.us-east-1.amazonaws.com/mqtt',
}));
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'lidar';
  coordinates = (data as any).default;
  // $lidarReport = new BehaviorSubject([]);
  cartesians = this.coordinates.message.map((polar:string) => {
    let coord = polar.split(":")
    let angle = parseFloat(coord[0])
    let radius = parseFloat(coord[1])
    let x = radius*Math.cos(degrees_to_radians(angle))/100
    let y = radius*Math.sin(degrees_to_radians(angle))/100
    return {x, y}
  });
  async ngOnInit() {
    Auth.currentCredentials().then((info) => {
      console.log(info)
      // const cognitoIdentityId = info.data.IdentityId;
    });
    PubSub.configure({
    });
    const coordinates = (data as any).default

    // await PubSub.publish("Lidar", "hola")
    PubSub.subscribe("Lidar").subscribe(message => {
      // this.$lidarReport.next(message)
      console.log(message)
      this.lineChartData = [
        {
          label: 'Scatter Dataset',
          data: message.value.message.map((dato: any) => polar_to_cartesian(dato)),
          borderColor: 'red'
        }
      ]
    })
  }
  lineChartData: ChartDataSets[] = [
    {
      label: 'Scatter Dataset',
      data: this.cartesians,
      borderColor: 'red'
    }
  ];

  lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June'];

  lineChartOptions = {
      animation: {
        duration: 0
      },
      scales: {
        xAxes: [{
          ticks: {
            min: -30,
            max: 30,
            stepSize: 1,
            callback: (v:any) => v == 0 ? '' : v
          },
          gridLines: {
            drawTicks: false
          }        
        }],
        yAxes: [{
          ticks: {
            min: -30,
            max: 30,
            stepSize: 1,
            callback: (v:any) => v == 0 ? '' : v
          },
          gridLines: {
            drawTicks: false
          } 
        }]
  }}

  lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,255,0,0.28)',
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [
    {
    beforeDraw: (chart: any) => {
      var xAxis = chart.scales['x-axis-1'];
      var yAxis = chart.scales['y-axis-1'];
      const scales = chart.chart.config.options.scales;
      scales.xAxes[0].ticks.padding = yAxis.top - yAxis.getPixelForValue(0) + 6;
      scales.yAxes[0].ticks.padding = xAxis.getPixelForValue(0) - xAxis.right + 6;
    }
  }
];
  lineChartType: ChartType = "scatter";
  
}



function degrees_to_radians(degrees: any)
{
  var pi = Math.PI;
  return degrees * (pi/180);
}
function polar_to_cartesian(polar: any) { 
    let coord = polar.split(":")
    let angle = parseFloat(coord[0])
    let radius = parseFloat(coord[1])
    let x = radius*Math.cos(degrees_to_radians(angle))/100
    let y = radius*Math.sin(degrees_to_radians(angle))/100
    return {x, y}
}
  
